﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Maliszewski
{
    public class Email
    {
        private MailMessage mm { get; set; }
        private string adress { get; set; } //adres na jaki chcemy wyslac
        private string html { get; set; }
        private string text_to_find {get; set;}


        public Email(string adress, string html, string text_to_find)
        {
            this.adress = adress;
            this.html = html;
            this.text_to_find = text_to_find;

        }


        public void wyslij_email()
        {
            //konfiguracja
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("iftttsystempwr@gmail.com", "ifttsystempwr");
            client.Timeout = 1000000000;
            LogJttt.LogMessage(System.Reflection.MethodBase.GetCurrentMethod().Name, "Email client init finished");

            //ustawianie tematu i wiadomsci
            this.mm = new MailMessage("iftttsystempwr@gmail.com", adress);
            this.mm.BodyEncoding = UTF8Encoding.UTF8;
            this.mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            this.mm.Subject = ("Hello this is your gag");
            BodyEmail();
            //wyslanie maila
            client.Send(this.mm);
            LogJttt.LogMessage(System.Reflection.MethodBase.GetCurrentMethod().Name, "Email send");
        }

        private void BodyEmail()
        {

            HtmlSample html = new HtmlSample(this.html);
            html.FindPageNodes(this.text_to_find);

            string src = html.FindPageNodes(this.text_to_find);

            string EmailBody = string.Format(@"Your text to find: {0} ", this.text_to_find);
            if (src != null)
            {
                WebClient client = new WebClient();
                ArrayList picture = new ArrayList();
                string imgString;
                string name = "Image";
                client.DownloadFile(src.ToString(), string.Format("{0}.jpg", name));

              
                LinkedResource image = new LinkedResource(Path.GetFullPath(string.Format("{0}.jpg", name)));
                image.ContentId = Guid.NewGuid().ToString();
                imgString = string.Format(@"<img src=""cid:{0}"" />", image.ContentId);
                EmailBody += imgString;

                picture.Add(image);
                
                var emailBodyView = AlternateView.CreateAlternateViewFromString(EmailBody, null, "text/html");
                emailBodyView.LinkedResources.Add(image);
                this.mm.AlternateViews.Add(emailBodyView);
                Console.WriteLine(src);

                //czyszci plik tymczasowy zeby moc wysylac wiecej niz jeden obrazek
                emailBodyView.LinkedResources.Clear();

               

            }
            else
            {
                this.mm.Body = "Anyting match";
            }
        }



    }
}
