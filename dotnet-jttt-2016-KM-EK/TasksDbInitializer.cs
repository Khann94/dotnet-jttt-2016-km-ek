﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maliszewski
{
    public class TasksDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<TasksDbContext>
    {
        protected override void Seed(TasksDbContext context)
        {

            Task Task = new Task("www.initialize.com", "text to find initialize", "initialize@gmail.com");
            context.Task.Add(Task);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
