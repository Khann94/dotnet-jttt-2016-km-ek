﻿using System.Data.Entity;

namespace Maliszewski
{
    public class TasksDbContext : DbContext
    {
        public TasksDbContext() : base("JTTTDb") {
            Database.SetInitializer(new TasksDbInitializer());
        }

        public DbSet<Task> Task { get; set; }

        //public DbSet <Task_List> Task_List { get; set; }
    }

}
