﻿using System;
using System.IO;

namespace Maliszewski
{
    class LogJttt
    {
        public static void LogError(string methodName, string message)
        {
            using (StreamWriter file = File.AppendText("jttt.log"))
            {
                file.WriteLine("{0} {1}", DateTime.Now.ToString(), DateTime.Now.ToLongDateString());
                file.WriteLine("{0} {1} {2}", "[Error]", methodName, message);
                file.WriteLine("------------------------");
            }
        }

        public static void LogWarning(string methodName, string message)
        {
            using (StreamWriter file = File.AppendText("jttt.log"))
            {
                file.WriteLine("{0} {1}", DateTime.Now.ToString(), DateTime.Now.ToLongDateString());
                file.WriteLine("{0} {1} {2}", "[Warning]", methodName, message);
                file.WriteLine("------------------------");
            }
        }

        public static void LogMessage(string methodName, string message)
        {
            using (StreamWriter file = File.AppendText("jttt.log"))
            {
                file.WriteLine("{0} {1}", DateTime.Now.ToString(), DateTime.Now.ToLongDateString());
                file.WriteLine("{0} {1} {2}", "[Message]", methodName, message);
                file.WriteLine("------------------------");
            }
        }
    }
}
