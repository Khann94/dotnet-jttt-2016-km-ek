﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using Newtonsoft.Json;

namespace Maliszewski
{

    public partial class Form1 : Form
    {
        //tutaj lista zadan dla widoku
        public Task_List tasklist = new Task_List() { name = DateTime.Now.ToString() };

        public Form1()
        {
            InitializeComponent();
            using (var ctx = new TasksDbContext())
            {
                foreach (var task in ctx.Task)
                {
                    Console.WriteLine("----------------");
                    Console.WriteLine(task);
                    tasklist.AddToList(task);
                    this.reloadList();

                }
            }


        }

        private void reloadList()
        {
            TaskListBox.DataSource = null;
            TaskListBox.DataSource = tasklist.getTask();
        }

        private bool Check_Empty_Box()
        {
            bool Empty = false;
            if (string.IsNullOrWhiteSpace(MailTextBox.Text))
            {
                LogJttt.LogWarning(System.Reflection.MethodBase.GetCurrentMethod().Name, "Empty mail box");
                //    MessageBox.Show("Mail text box can't be empty");
                Empty = true;
            }
            if (string.IsNullOrWhiteSpace(TextFindBox.Text))
            {
                LogJttt.LogWarning(System.Reflection.MethodBase.GetCurrentMethod().Name, "Empty find text box");
                //  MessageBox.Show("Text box can't be empty");
                Empty = true;
            }
            if (string.IsNullOrWhiteSpace(URLBox.Text))
            {
                LogJttt.LogWarning(System.Reflection.MethodBase.GetCurrentMethod().Name, "Empty URL box");
                //    MessageBox.Show("Empty URL box");
                Empty = true;
            }
            else
            {
                if (URL_Help.checkURL(URLBox.Text))
                {
                    URL_Label.Text = string.Empty;
                }
                else
                {
                    URLBox.Text = "Nieprawidlowy adres URL";
                    LogJttt.LogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "URL is invalid");
                }
            }
            return Empty;
        }
        //publiczna metoda zwracjaca tekst URL_BOX
        public string getTekstURL()
        {
            return URLBox.Text;
        }

        //zwraca tekst ktory jets publiczny
        public string getTekstFind()
        {
            return TextFindBox.Text;
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            //jesli jest obrazek zawierajacy dany opis to wyslij wiadomosc
            WarunekTekstNaStronie war_txt_na_stronie = new WarunekTekstNaStronie(this);
            bool spelniony = war_txt_na_stronie.SprawdzWarunek();

            if (spelniony)
            {
                LogJttt.LogMessage(System.Reflection.MethodBase.GetCurrentMethod().Name, "Image find");

                string adress = MailTextBox.Text;
                string html = URLBox.Text;
                string text_to_find = TextFindBox.Text;

                Email email = new Email(adress, html, text_to_find);
                email.wyslij_email();

                MessageBox.Show("Wysłano wiadomosc na adres: " + adress + "\nSTRONA: " + html
                     + "\nSZUKANY TEKST: " + text_to_find);
                LogJttt.LogMessage(System.Reflection.MethodBase.GetCurrentMethod().Name, "SCR: " + war_txt_na_stronie.getZnaleziony());
                LogJttt.LogMessage(System.Reflection.MethodBase.GetCurrentMethod().Name, "Email send");

            }
            else
            {
                MessageBox.Show("Na podanej stronie nie ma obrazka zawierajacego podany opis");
                LogJttt.LogMessage(System.Reflection.MethodBase.GetCurrentMethod().Name, "Can not find image");
            }
        }



        private void AddButton_Click(object sender, EventArgs e)
        {
            if (!this.Check_Empty_Box())
            {
                Task task = new Task(URLBox.Text, TextFindBox.Text, MailTextBox.Text);
                tasklist.AddToList(task);

                tasklist.Tasks.Add(task);
                using (var ctx = new TasksDbContext())
                {
                    ////baza danych - dodanie do bazy 
                    ctx.Task.Add(task);
                    ctx.SaveChanges();
                }

                this.reloadList();
            }
        }

        private void SerializeButton_Click(object sender, EventArgs e)
        {
            if (TaskListBox.Items.Count > 0)
            {
                string path = Directory.GetCurrentDirectory() + @".bin";
                using (FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binary = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    binary.Serialize(fileStream, tasklist);
                    fileStream.Close();
                }
                this.reloadList();
            }

        }

        private void DeserializeButton_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory() + @".bin";
            using (FileStream ds = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binary = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                Task_List p = binary.Deserialize(ds) as Task_List;

                this.tasklist = p;
                this.reloadList();
            }

        }

        private void OpenPageButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(URLBox.Text))
            {
                URL_Label.Text = "Pusty URL";
                LogJttt.LogWarning(System.Reflection.MethodBase.GetCurrentMethod().Name, "URL text box is empty");
            }
            else
            {
                if (URL_Help.checkURL(URLBox.Text))
                {
                    Process.Start(URLBox.Text);
                    URL_Label.Text = string.Empty;
                }
                else
                {
                    URL_Label.Text = "Nieprawidlowy URL";
                    LogJttt.LogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "URL text box is invalid");
                }
            }
        }


        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.TaskListBox.Items.Count > 0)
            {
                this.tasklist.RemoveWithIndex(this.TaskListBox.SelectedIndex);

                //Task task = new Task(this.TaskListBox.SelectedIndex);
                //using (var ctx = new TasksDbContext())
                //{
                //    ctx.Task.Attach(task);
                //    ctx.Task.Remove(task);
                //    ctx.SaveChanges();
                //}

                var task = ((Task)this.TaskListBox.SelectedItem);
                using (var ctx = new TasksDbContext())
                {
                    ctx.Task.Attach(task);
                    ctx.Task.Remove(task);

                    bool saveFailed;
                    do
                    {
                        saveFailed = false;
                        try
                        {
                            ctx.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;
                            var entry = ex.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());

                        }
                    } while (saveFailed);
                }
            }
        }

        private void RealizeButton_Click(object sender, EventArgs e)
        {
            WarunekTekstNaStronie war_txt_na_stronie = new WarunekTekstNaStronie(this);
            if (this.TaskListBox.Items.Count > 0)
            {
                Task task = (Task)this.TaskListBox.SelectedItem;

                string adress = MailTextBox.Text;
                string html = URLBox.Text;
                string text_to_find = TextFindBox.Text;

                Email email = new Email(adress, html, text_to_find);
                email.wyslij_email();
                MessageBox.Show("Email na podany adres " + adress + " zostal wyslany");
            }
        }

        private void PogodaButton_Click(object sender, EventArgs e)
        {
            Pogoda check = new Pogoda();
            string responseFromServer = check.Request(TownBox.Text);

            Pogoda.PrognozaPogody pogoda = JsonConvert.DeserializeObject<Pogoda.PrognozaPogody>(responseFromServer);

            string danePogodowe = $"Temperatura w miescie {TownBox.Text} wynosi {pogoda.Main.Temp - 273} stopni Celsjusza. Wilgotnosc jest na poziomie  {pogoda.Main.Humidity}% a cisnienie atmosferyczne {pogoda.Main.Pressure} hPa. ";
            MessageBox.Show(danePogodowe);
        }
    }
}
