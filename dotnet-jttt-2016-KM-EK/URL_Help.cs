﻿using System;

namespace Maliszewski
{
    public static  class URL_Help
    {
        //Sprawdzamy czy podany URL jest prawidlowy
        public static bool checkURL(string url)
        {
            Uri result;
            return Uri.TryCreate(url, UriKind.Absolute, out result) && (result.Scheme == Uri.UriSchemeHttp || result.Scheme == Uri.UriSchemeHttps);
        }
    }
}
