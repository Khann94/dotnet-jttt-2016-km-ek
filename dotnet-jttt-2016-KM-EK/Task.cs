﻿using System;

namespace Maliszewski
{
    [Serializable]

    public class Task
    { 
        public int Id { get; set; }
        public string URLstring { get; set; }
        public string Text_To_Find_String { get; set; }
        public string EmailString { get; set; }

        public int? Task_ListId { get; set; }

        //[ForeignKey("Task_ListId")]

        public virtual Task_List TaskList {get; set; }

        public Task(string URL, string text_to_find, string email)
        {
            this.URLstring = URL;
            this.Text_To_Find_String = text_to_find;
            this.EmailString = email;
        }

        public Task(int id)
        {
            this.Id = id;
        }

        public Task()
        {

        }

        public override string ToString()
        {
            string taskDesc = string.Format(@"{0}, {1}, {2},{3}",this.Id, this.URLstring, this.Text_To_Find_String, this.EmailString);
            return taskDesc;
        }
    }

}
