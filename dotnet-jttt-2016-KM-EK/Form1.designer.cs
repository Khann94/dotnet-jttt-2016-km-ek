﻿using System;

namespace Maliszewski
{
    public partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextFindBox = new System.Windows.Forms.TextBox();
            this.SendButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.MailTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.URLBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TaskListBox = new System.Windows.Forms.ListBox();
            this.OpenPageButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.SerializeButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.DeserializeButton = new System.Windows.Forms.Button();
            this.URL_Label = new System.Windows.Forms.Label();
            this.RealizeButton = new System.Windows.Forms.Button();
            this.jTTTDb = new dotnet_jttt_2016_KM_EK.JTTTDb();
            this.jTTTDbBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.jTTTDbBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.jTTTDbBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TabPage = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.TempBox = new System.Windows.Forms.TextBox();
            this.PogodaButton = new System.Windows.Forms.Button();
            this.TownBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDbBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDbBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDbBindingSource2)).BeginInit();
            this.TabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.TabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextFindBox
            // 
            this.TextFindBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextFindBox.Location = new System.Drawing.Point(11, 77);
            this.TextFindBox.Name = "TextFindBox";
            this.TextFindBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextFindBox.Size = new System.Drawing.Size(239, 23);
            this.TextFindBox.TabIndex = 0;
            this.TextFindBox.Text = "Nie";
            // 
            // SendButton
            // 
            this.SendButton.Location = new System.Drawing.Point(377, 154);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(68, 36);
            this.SendButton.TabIndex = 1;
            this.SendButton.Text = "Wyślij!";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "znajduje się obrazek, którego podpis zawiera tekst:";
            // 
            // MailTextBox
            // 
            this.MailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MailTextBox.Location = new System.Drawing.Point(11, 139);
            this.MailTextBox.Name = "MailTextBox";
            this.MailTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MailTextBox.Size = new System.Drawing.Size(285, 23);
            this.MailTextBox.TabIndex = 4;
            this.MailTextBox.Text = "k.maliszewski94@gmail.com";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(286, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "to na podany adres e-mail wyślij wiadomość:";
            // 
            // URLBox
            // 
            this.URLBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.URLBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.URLBox.Location = new System.Drawing.Point(11, 23);
            this.URLBox.Name = "URLBox";
            this.URLBox.Size = new System.Drawing.Size(244, 16);
            this.URLBox.TabIndex = 6;
            this.URLBox.Text = "http://www.demotywatory.pl";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Jeśli na stronie";
            // 
            // TaskListBox
            // 
            this.TaskListBox.BackColor = System.Drawing.Color.White;
            this.TaskListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TaskListBox.ForeColor = System.Drawing.Color.Black;
            this.TaskListBox.FormattingEnabled = true;
            this.TaskListBox.Location = new System.Drawing.Point(377, 16);
            this.TaskListBox.Name = "TaskListBox";
            this.TaskListBox.Size = new System.Drawing.Size(338, 132);
            this.TaskListBox.TabIndex = 27;
            // 
            // OpenPageButton
            // 
            this.OpenPageButton.Location = new System.Drawing.Point(375, 196);
            this.OpenPageButton.Name = "OpenPageButton";
            this.OpenPageButton.Size = new System.Drawing.Size(70, 36);
            this.OpenPageButton.TabIndex = 28;
            this.OpenPageButton.Text = "Otworz Strone";
            this.OpenPageButton.UseVisualStyleBackColor = true;
            this.OpenPageButton.Click += new System.EventHandler(this.OpenPageButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(456, 196);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(87, 36);
            this.DeleteButton.TabIndex = 31;
            this.DeleteButton.Text = "Usun";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // SerializeButton
            // 
            this.SerializeButton.Location = new System.Drawing.Point(630, 154);
            this.SerializeButton.Name = "SerializeButton";
            this.SerializeButton.Size = new System.Drawing.Size(85, 36);
            this.SerializeButton.TabIndex = 32;
            this.SerializeButton.Text = "Serializacja";
            this.SerializeButton.UseVisualStyleBackColor = true;
            this.SerializeButton.Click += new System.EventHandler(this.SerializeButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(456, 154);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(87, 36);
            this.AddButton.TabIndex = 33;
            this.AddButton.Text = "Dodaj";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DeserializeButton
            // 
            this.DeserializeButton.Location = new System.Drawing.Point(630, 196);
            this.DeserializeButton.Name = "DeserializeButton";
            this.DeserializeButton.Size = new System.Drawing.Size(85, 36);
            this.DeserializeButton.TabIndex = 34;
            this.DeserializeButton.Text = "Deserializacja";
            this.DeserializeButton.UseVisualStyleBackColor = true;
            this.DeserializeButton.Click += new System.EventHandler(this.DeserializeButton_Click);
            // 
            // URL_Label
            // 
            this.URL_Label.Location = new System.Drawing.Point(14, 32);
            this.URL_Label.Name = "URL_Label";
            this.URL_Label.Size = new System.Drawing.Size(250, 13);
            this.URL_Label.TabIndex = 35;
            // 
            // RealizeButton
            // 
            this.RealizeButton.Location = new System.Drawing.Point(549, 154);
            this.RealizeButton.Name = "RealizeButton";
            this.RealizeButton.Size = new System.Drawing.Size(75, 78);
            this.RealizeButton.TabIndex = 36;
            this.RealizeButton.Text = "Wykonaj";
            this.RealizeButton.UseVisualStyleBackColor = true;
            this.RealizeButton.Click += new System.EventHandler(this.RealizeButton_Click);
            // 
            // jTTTDb
            // 
            this.jTTTDb.DataSetName = "JTTTDb";
            this.jTTTDb.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // jTTTDbBindingSource
            // 
            this.jTTTDbBindingSource.DataSource = this.jTTTDb;
            this.jTTTDbBindingSource.Position = 0;
            // 
            // jTTTDbBindingSource1
            // 
            this.jTTTDbBindingSource1.DataSource = this.jTTTDb;
            this.jTTTDbBindingSource1.Position = 0;
            // 
            // jTTTDbBindingSource2
            // 
            this.jTTTDbBindingSource2.DataSource = this.jTTTDb;
            this.jTTTDbBindingSource2.Position = 0;
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.tabPage1);
            this.TabControl1.Controls.Add(this.TabPage);
            this.TabControl1.Location = new System.Drawing.Point(2, 0);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(367, 207);
            this.TabControl1.TabIndex = 37;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.SteelBlue;
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.URLBox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.TextFindBox);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.MailTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(359, 181);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // TabPage
            // 
            this.TabPage.BackColor = System.Drawing.Color.DarkSlateGray;
            this.TabPage.Controls.Add(this.label5);
            this.TabPage.Controls.Add(this.TempBox);
            this.TabPage.Controls.Add(this.PogodaButton);
            this.TabPage.Controls.Add(this.TownBox);
            this.TabPage.Controls.Add(this.label2);
            this.TabPage.Location = new System.Drawing.Point(4, 22);
            this.TabPage.Name = "TabPage";
            this.TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage.Size = new System.Drawing.Size(359, 181);
            this.TabPage.TabIndex = 1;
            this.TabPage.Text = "Pogoda";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(8, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(316, 22);
            this.label5.TabIndex = 5;
            this.label5.Text = "Podaj temperature ";
            // 
            // TempBox
            // 
            this.TempBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TempBox.Location = new System.Drawing.Point(59, 93);
            this.TempBox.Name = "TempBox";
            this.TempBox.Size = new System.Drawing.Size(207, 23);
            this.TempBox.TabIndex = 4;
            this.TempBox.Text = "10";
            // 
            // PogodaButton
            // 
            this.PogodaButton.Location = new System.Drawing.Point(59, 131);
            this.PogodaButton.Name = "PogodaButton";
            this.PogodaButton.Size = new System.Drawing.Size(78, 38);
            this.PogodaButton.TabIndex = 3;
            this.PogodaButton.Text = "Wyswietl Pogode";
            this.PogodaButton.UseVisualStyleBackColor = true;
            this.PogodaButton.Click += new System.EventHandler(this.PogodaButton_Click);
            // 
            // TownBox
            // 
            this.TownBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TownBox.Location = new System.Drawing.Point(59, 42);
            this.TownBox.Name = "TownBox";
            this.TownBox.Size = new System.Drawing.Size(207, 23);
            this.TownBox.TabIndex = 1;
            this.TownBox.Text = "Wroclaw";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(19, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(316, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Podaj miasto w ktorym chcesz sprawdzic pogode";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(727, 333);
            this.Controls.Add(this.TabControl1);
            this.Controls.Add(this.RealizeButton);
            this.Controls.Add(this.URL_Label);
            this.Controls.Add(this.DeserializeButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.SerializeButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.OpenPageButton);
            this.Controls.Add(this.TaskListBox);
            this.Controls.Add(this.SendButton);
            this.Name = "Form1";
            this.Text = "IFTT v2.0";
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDbBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDbBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jTTTDbBindingSource2)).EndInit();
            this.TabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.TabPage.ResumeLayout(false);
            this.TabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextFindBox;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MailTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox URLBox;
        private System.Windows.Forms.ListBox TaskListBox;
        private System.Windows.Forms.Button OpenPageButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button SerializeButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button DeserializeButton;
        private System.Windows.Forms.Label URL_Label;
        private System.Windows.Forms.Button RealizeButton;
        private System.Windows.Forms.BindingSource jTTTDbBindingSource;
        private dotnet_jttt_2016_KM_EK.JTTTDb jTTTDb;
        private System.Windows.Forms.BindingSource jTTTDbBindingSource1;
        private System.Windows.Forms.BindingSource jTTTDbBindingSource2;
        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage TabPage;
        private System.Windows.Forms.TextBox TownBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button PogodaButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TempBox;
    }
}

