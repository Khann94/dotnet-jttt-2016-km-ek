﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maliszewski
{
    //abstrakcyjna klasa Warunek
    public abstract class Warunek
    {
        public Form1 form1;

        protected Warunek(Form1 form1)
        {
            this.form1 = form1;
        }
        abstract public bool SprawdzWarunek();
    }

    //Klasa dziedziczaca po klasie Warunek
    //i nadpisuje metode sprwdz warunek
    public class WarunekTekstNaStronie : Warunek
    {
        private string znaleziony;

        public WarunekTekstNaStronie(Form1 form1) : base(form1) { }

        public override bool SprawdzWarunek()
        {
            var hs = new HtmlSample(form1.getTekstURL());
            znaleziony = hs.FindPageNodes(form1.getTekstFind());

            if (znaleziony != null)
            {
                return true;
            }
            return false;
        }

        public string getZnaleziony()
        {
            return this.znaleziony;
        }

    }
}
