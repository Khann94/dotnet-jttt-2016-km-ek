namespace dotnet_jttt_2016_KM_EK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    URLstring = c.String(),
                    Text_To_Find_String = c.String(),
                    EmailString = c.String(),
                    Task_ListId = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Task_List", t => t.Task_ListId)
                .Index(t => t.Task_ListId);

            CreateTable(
                "dbo.Task_List",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "Task_ListId", "dbo.Task_List");
            DropIndex("dbo.Tasks", new[] { "Task_ListId" });
            DropTable("dbo.Task_List");
            DropTable("dbo.Tasks");
        }
    }
}
