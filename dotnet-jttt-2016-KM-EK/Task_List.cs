﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Maliszewski
{
    [Serializable]
    public class Task_List
    {
        public int Id { get; set; }
        public string name { get; set; }

        //dla wyswietlania w formularzu
        private BindingList<Task> tasklist { get; set; }

        //dla bazy danych
        public virtual List<Task> Tasks { get; set; } = new List<Task>();

        public Task_List()
        {
            name = null;
            tasklist = new BindingList<Task>();

            tasklist.AllowNew = true;
            tasklist.AllowRemove = true;
            tasklist.RaiseListChangedEvents = true;
            tasklist.AllowNew = true;
        }
        
        public void AddToList(Task newTask)
        {
            tasklist.Add(newTask);
        }

        public void RemoveWithIndex(int index)
        {
            tasklist.RemoveAt(index);
        }

        public BindingList<Task> getTask()
        {
            return tasklist;
        }

        public void clear()
        {
            tasklist.Clear();
        }

        public override string ToString()
        {
            string taskListString = string.Format(@"{0}", this.name);
            return taskListString;
            
        }
    }
}
