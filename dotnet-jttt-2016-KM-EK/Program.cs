﻿using System;
using System.Windows.Forms;

namespace Maliszewski
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            ////odwolanie do bazy danych przy uzyciu obiektu kontekstu

            ////lista inicjalizacyjna bo DBInitilizer nie dziala
            //Task_List dblist = new Task_List() { name = "Lita Zadan I" };

            //Task Task = new Task("www.initialize.com", "text to find initialize", "initialize@gmail.com");
            //dblist.Tasks.Add(Task);

            //ctx.Task_List.Add(dblist);

            //ctx.SaveChanges(); 

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }
    }
}
